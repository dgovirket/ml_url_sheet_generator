<?php

$envFile = __DIR__.'/.env';

if ( !file_exists($envFile) ) {
    trigger_error('File not found .env', E_USER_ERROR);
}

$lines = explode(PHP_EOL, file_get_contents($envFile));

foreach($lines as $line) {

    if ( empty($line) )
        continue;

    list($var, $value) = explode('=', $line, 2);

    putenv("$var=$value");

}