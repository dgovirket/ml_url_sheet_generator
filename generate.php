<?php

require __DIR__ . '/autoload.php';
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/Controllers/MultilocationController.php';

if (php_sapi_name() != 'cli') {
    throw new Exception('This application must be run on the command line.');
}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */

function getClient() {

    $client = new Google_Client();

    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.

    $tokenPath = 'token.json';

    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.

    if ($client->isAccessTokenExpired()) {

        // Refresh the token if possible, else fetch a new one.

        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }

        // Save the token to a file.

        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }

        file_put_contents($tokenPath, json_encode($client->getAccessToken()));

    }

    return $client;
}

// Get the API client and construct the service object.

$client = getClient();
$service = new Google_Service_Sheets($client);

// Config

$spreadsheetId = getenv('SPREADSHEET_ID');
$range = getenv('RANGE_GET');

// Get Data

$response = $service->spreadsheets_values->get($spreadsheetId, $range);
$values = $response->getValues();

if (empty($values)) {

    print "No data found.\n";

} else {

    if (getenv('SHOW_URLS_GENERATED') == 'true') {
        print "URLs Generated: \n\n";
    }

    $ml = new MultilocationController;

    $ml->debug = (getenv('DEBUG') == 'true') ? true : false;

    $ml->url = getenv('SITE_URL');

    $urls = array();
    foreach ($values as $row) {

        $ml->sucursal = $row[0];
        $ml->estado = $row[2];
        $ml->municipio = $row[3];
        $ml->colonia = $row[4];

        $url = $ml->getUrl();

        $urls[] = array($url);

        if (getenv('SHOW_URLS_GENERATED') == 'true') {
            print "$url \n";
        }

    }

}

// Write Data

if (getenv('UPDATE_DATA') == 'true') {

    $range = getenv('RANGE_UPDATE');

    $values = $urls;

    $requestBody = new Google_Service_Sheets_ValueRange();
    $requestBody->setValues($values);

    $conf = ['valueInputOption' => "RAW"];

    $response = $service->spreadsheets_values->update($spreadsheetId, $range, $requestBody, $conf);

    if (getenv('DEBUG') == 'true') {
        print "\n";
        echo '<pre>', var_export($response, true), '</pre>', "\n";
    }
}

print "\nDone";