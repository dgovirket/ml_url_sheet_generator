<?php

class MultilocationController {

	public $sucursal;
    public $estado;
    public $municipio;
    public $colonia;

    public $url;
    public $debug;

    function __construct() {
    	$this->debug = false;
    }

	function getUrl() {

		$sucursal = $this->getSlug($this->sucursal);
		$estado = $this->getSlugState($this->estado);
		$municipio = $this->getSlug($this->municipio);
		$colonia = $this->getSlug($this->colonia);

		$randText = $this->getRandText(7);

		if ($this->debug) {
			print "$this->sucursal :: $this->estado :: $this->municipio :: $this->colonia \n";
			print "$sucursal :: $estado :: $municipio :: $colonia \n";
		}

		$url = str_replace('{randText}', $randText, $this->url);
		$url .= '/'.$estado.'/'.$municipio.'/'.$colonia.'/'.$sucursal.'/';

		if ($this->debug) {
			print $url;
			print "\n\n";
		}

		return $url;

	}

	function getSlug($string) {

	    $slug = $this->replaceChars($string);
	    $slug = preg_replace('/[^ A-z0-9\-]+/', '', $slug);
	    $slug = strtolower(trim($slug));
	    $slug = str_replace(' ', '-', $slug);

	    return $slug;
	}

	function getSlugState($state) {

	    if ($state == 'AGU') {
	        $state = 'aguascalientes';
	    } elseif ($state == 'BCN') {
	        $state = 'baja-california-norte';
	    } elseif ($state == 'BCS') {
	        $state = 'baja-california-sur';
	    } elseif ($state == 'CAM') {
	        $state = 'campeche';
	    } elseif ($state == 'CHH') {
	        $state = 'chihuahua';
	    } elseif ($state == 'CHP') {
	        $state = 'chiapas';
	    } elseif ($state == 'CMX') {
	        $state = 'cdmx';
	    } elseif ($state == 'COL') {
	        $state = 'colima';
	    } elseif ($state == 'COA') {
	        $state = 'coahuila';
	    } elseif ($state == 'DUR') {
	        $state = 'durango';
	    } elseif ($state == 'GRO') {
	        $state = 'guerrero';
	    } elseif ($state == 'GUA') {
	        $state = 'guanajuato';
	    } elseif ($state == 'HID') {
	        $state = 'hidalgo';
	    } elseif ($state == 'JAL') {
	        $state = 'jalisco';
	    } elseif ($state == 'MIC') {
	        $state = 'michoacan';
	    } elseif ($state == 'MEX') {
	        $state = 'estado-de-mexico';
	    } elseif ($state == 'MOR') {
	        $state = 'morelos';
	    } elseif ($state == 'NAY') {
	        $state = 'nayarit';
	    } elseif ($state == 'NLE') {
	        $state = 'nuevo-leon';
	    } elseif ($state == 'OAX') {
	        $state = 'oaxaca';
	    } elseif ($state == 'PUE') {
	        $state = 'puebla';
	    } elseif ($state == 'QRO') {
	        $state = 'queretaro';
	    } elseif ($state == 'QUE') {
	        $state = 'queretaro';
	    } elseif ($state == 'ROO') {
	        $state = 'quintana-roo';
	    } elseif ($state == 'SIN') {
	        $state = 'sinaloa';
	    } elseif ($state == 'SLP') {
	        $state = 'san-luis-potosi';
	    } elseif ($state == 'SON') {
	        $state = 'sonora';
	    } elseif ($state == 'TAB') {
	        $state = 'tabasco';
	    } elseif ($state == 'TAM') {
	        $state = 'tamaulipas';
	    } elseif ($state == 'TLA') {
	        $state = 'tlaxcala';
	    } elseif ($state == 'VER') {
	        $state = 'veracruz';
	    } elseif ($state == 'YUC') {
	        $state = 'yucatan';
	    } elseif ($state == 'ZAC') {
	        $state = 'zacatecas';
	    } else {
	        $state = strtolower($state);
	    }

	    return $state;
	}

	function getRandText($lenght) {

	    $varchars = '0123456789abcdefghijklmnopqrstuvwxyz';

	    $varchars_length = strlen($varchars);

	    $random_string = '';
	    for($i = 0; $i < $lenght; $i++) {
	        $random_character = $varchars[mt_rand(0, $varchars_length - 1)];
	        $random_string .= $random_character;
	    }

	    return $random_string;
	}

	function replaceChars($string) {

	    $string = str_replace(
	        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
	        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
	        $string
	    );

	    $string = str_replace(
	        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
	        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
	        $string
	    );

	    $string = str_replace(
	        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
	        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
	        $string
	    );

	    $string = str_replace(
	        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
	        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
	        $string
	    );

	    $string = str_replace(
	        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
	        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
	        $string
	    );

	    $string = str_replace(
	        array('ñ', 'Ñ', 'ç', 'Ç'),
	        array('n', 'N', 'c', 'C',),
	        $string
	    );

	    return $string;
	}

}