# Multilocation Url Sheet Generator

Multilocation Url Sheet Generator is an application used with Google Spreadsheets to generate public URLs in slug format.

Version 1.0

### Installation

This application require:

* Php >= 5.4
* [Google Api Client](https://github.com/googleapis/google-api-php-client) >= 2.0.

Run the follow in console.

```sh
$ cd ml_url_sheet_generator
$ composer install
```
After run then:

* Rename the file ".env.example" to ".env".
* Edit the file ".env" and assign value to the next vars.
        > SPREADSHEET_ID: Is the SpreadSheet ID. This val can be get in the url.
        > RANGE_GET: Is the range in the spreadsheet where it's get the data, branch name, state, city, borough and neighborhood. Example: Sheet1!F2:J10.
        > RANGE_UPDATE: Is the range in the spreadsheet where the urls generated be copied. Example: Sheet1!E2.
        > SITE_URL: Is the url that will be used for generate the full url.
        > UPDATE_DATA: Is a boolean value that it's set to true then the process will update the spreadsheet range with the url generated.
        > SHOWS_URL_GENERATED: Is a boolean value that it's set to true then the process will show de urls generated in console.
        > DEBUG: Is a boolean value that it's set to true then it will show the message from Google Spreadsheet API.
* Rename the file "credentials.json.example" to "credentials.json".
* This file need the OAuth credentials values generated in the [Google Console](https://console.cloud.google.com) with type Others.
* In Google Console enable the Google Sheets API.
* Edit the file "credentials.json" and set values to the follow vars:
        > client_id
        > project_id
        > client_secret
* When all configuration is set then run the follow in console:

```sh
$ php generate.php
```
